#pragma once
#include <Logic/Point.hpp>
#include <algorithm>
#include <list>
#include <maybe.hpp>

class Snake {
public:
  Snake(unsigned int, unsigned int);

  std::pair<const Snake, const bool> moveOrGrow(const Point &,
                                                const Point &) const;

  bool eats(const Point &) const;

  bool selfIntersecting() const;

  maybe<Point> nextPrey() const;

  const std::list<Point> &getSegments() const;

private:
  const unsigned int xBound;
  const unsigned int yBound;
  std::list<Point> segments;

  Point nextHead(const Point &) const;
};