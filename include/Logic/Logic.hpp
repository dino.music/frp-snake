#pragma once
#include <Logic/Direction.hpp>
#include <Logic/Snake.hpp>
#include <ctype.h>
#include <millisecond_timer_system_impl.hpp>
#include <sodium/sodium.h>

class Logic {
public:
  Logic(const sodium::timer_system<std::chrono::milliseconds> &,
        const sodium::stream<Direction> &, const sodium::stream<sodium::unit> &,
        const sodium::cell<bool> &);

  const sodium::cell<int> &score() const { return score_; }

  const sodium::cell<Snake> &snake() const { return snake_; }

  const sodium::cell<Point> &prey() const { return prey_; }

  const sodium::cell<bool> &terminated() const { return terminated_; }

  constexpr static int fieldWidth = 40;
  constexpr static int fieldHeight = 20;

private:
  const sodium::timer_system<std::chrono::milliseconds> &sys_;
  sodium::cell<int> score_;
  sodium::cell<Snake> snake_;
  sodium::cell<Point> prey_;
  sodium::cell<bool> terminated_;

  const sodium::stream<std::chrono::milliseconds>
  ticker(const sodium::cell<std::chrono::milliseconds>) const;

  const sodium::stream<Point>
  movementTicks(const sodium::stream<Direction> &,
                const sodium::stream<sodium::unit> &,
                const sodium::cell<bool> &) const;
};