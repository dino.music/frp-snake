#pragma once
#include <Logic/Point.hpp>

enum class Direction { Up, Right, Down, Left };

const Point directionToPoint(const Direction);