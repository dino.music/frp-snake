#pragma once

struct Point {
  Point(int, int);

  const Point operator+(const Point &) const;

  bool operator==(const Point &) const;

  const Point reverse() const;

  const int x;
  const int y;
};

extern const Point UP;
extern const Point DOWN;
extern const Point RIGHT;
extern const Point LEFT;