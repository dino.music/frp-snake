#pragma once

#include <termbox.h>

struct TBGuard {
  TBGuard() { tb_init(); }
  ~TBGuard() { tb_shutdown(); }
};