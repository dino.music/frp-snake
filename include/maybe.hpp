#pragma once

#include <boost/optional.hpp>

template <typename T> using maybe = boost::optional<T>;