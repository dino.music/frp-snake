#pragma once
#include <Logic/Logic.hpp>

struct RenderData {
  static RenderData calcData(const int, const int);
  //+2 for walls, which are one block size
  // two walls N and S, two walls E and W
  constexpr static int frameWidth = Logic::fieldWidth + 2;
  constexpr static int frameHeight = Logic::fieldHeight + 2;
  constexpr static int headerHeight = 3;
  int w = Logic::fieldWidth;
  int h = Logic::fieldHeight;
  int offsetX = 1;
  int offsetY = 1;
  int cellsPerBlockY = 1;
  int cellsPerBlockX = 1;
  bool displaySymbol = false;
  bool valid = true;
  Direction orientation = Direction::Up;

private:
  RenderData() = default;
  void initData(const int, const int, const int, const int);
};