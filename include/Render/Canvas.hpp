#pragma once
#include <Render/TBRenderer.hpp>
#include <sodium/sodium.h>
#include <unordered_map>

using Dimensions = std::pair<int, int>;

class Canvas {
public:
  Canvas(const sodium::stream<Dimensions> &, const Dimensions &);

  const sodium::cell<TBRenderer> &renderer() const { return renderer_; }

  const sodium::cell<bool> &valid() const { return valid_; }

  const sodium::cell<Direction> &orientation() const { return orientation_; }

private:
  sodium::cell<TBRenderer> renderer_;
  sodium::cell<bool> valid_;
  sodium::cell<Direction> orientation_;
};