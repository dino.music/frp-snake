#pragma once

#include <Render/RenderData.hpp>

class TBRenderer {
public:
  TBRenderer(const RenderData &d) : data{d} {}

  void drawBlock(const int _, const int, const int, const char) const;

  void drawWalls() const;

  void drawHeader(const int) const;

private:
  RenderData data;

  void drawWall(const int, const int, const int, const int) const;
};