#pragma once
#include <Logic/Logic.hpp>
#include <Render/Canvas.hpp>
#include <functional>

class Game {
public:
  Game(const sodium::stream<char> &, const sodium::stream<Dimensions> &);

  const sodium::cell<std::function<void()>> &render() const { return render_; }

private:
  const sodium::timer_system<std::chrono::milliseconds> sys_;
  sodium::stream<Direction> instructions_;
  sodium::stream<sodium::unit> pause_;
  Canvas canvas_;
  sodium::cell<std::function<void()>> render_;
  static const std::unordered_map<char, Direction> standardMapping;
  static const std::unordered_map<char, Direction> rotatedMapping;
};