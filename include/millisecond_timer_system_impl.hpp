#pragma once

#include <atomic>
#include <chrono>
#include <memory>
#include <sodium/time.h>
#include <thread>

class millisecond_timer_system_impl
    : public sodium::timer_system_impl<std::chrono::milliseconds> {
private:
  struct entry {
    entry(std::chrono::milliseconds t_, std::function<void()> callback_,
          long long seq_)
        : t(t_), callback(callback_), seq(seq_) {}
    bool operator<(const entry &other) const {
      if (t < other.t)
        return true;
      if (t > other.t)
        return false;
      return seq < other.seq;
    }
    bool operator==(const entry &other) const { return seq == other.seq; }

    std::chrono::milliseconds t;
    std::function<void()> callback;
    long long seq;
  };

public:
  millisecond_timer_system_impl();

  virtual ~millisecond_timer_system_impl();

  std::function<void()> set_timer(std::chrono::milliseconds t,
                                  std::function<void()> callback) override;

  std::chrono::milliseconds now() override;

private:
  sodium::impl::thread_safe_priority_queue<entry> entries;
  std::thread t;
  std::atomic<bool> finished{false};
  std::atomic<std::chrono::milliseconds> now_{time()};
  std::chrono::milliseconds initTime_;
  long long next_seq;

  std::chrono::milliseconds time() const;
};