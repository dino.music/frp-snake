#include <Logic/Snake.hpp>

Snake::Snake(unsigned int x, unsigned int y) : xBound{x}, yBound{y} {
  for (auto i = 0u; i < 5; ++i)
    segments.emplace_back(i, 1);
}

std::pair<const Snake, const bool> Snake::moveOrGrow(const Point &d,
                                                     const Point &p) const {
  auto newSnake = *this;
  newSnake.segments.push_back(nextHead(d));
  bool e = eats(p);
  if (!e)
    newSnake.segments.pop_front();
  return std::make_pair(newSnake, e);
}

bool Snake::eats(const Point &p) const { return segments.back() == p; }

bool Snake::selfIntersecting() const {
  return std::any_of(segments.cbegin(), --segments.cend(),
                     [this](const auto &p) { return eats(p); });
}

maybe<Point> Snake::nextPrey() const {
  // be generated in that situation
  const auto maxLength = xBound * yBound;
  if (maxLength == segments.size())
    return maybe<Point>();
  while (true) {
    Point p(std::rand() % xBound, std::rand() % yBound);
    if (std::find(segments.cbegin(), segments.cend(), p) == segments.cend())
      return p;
  }
}

const std::list<Point> &Snake::getSegments() const { return segments; }

Point Snake::nextHead(const Point &d) const {
  Point nh = segments.back() + d;
  return Point(nh.x < 0 ? (xBound - 1) : (nh.x % xBound),
               nh.y < 0 ? (yBound - 1) : (nh.y % yBound));
}