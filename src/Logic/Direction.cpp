#include <Logic/Direction.hpp>

const Point directionToPoint(const Direction d) {
  if (d == Direction::Up)
    return UP;
  else if (d == Direction::Down)
    return DOWN;
  else if (d == Direction::Left)
    return LEFT;
  else
    return RIGHT;
}