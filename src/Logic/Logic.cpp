#include <Logic/Logic.hpp>

template <typename T> using maybe = boost::optional<T>;

Logic::Logic(const sodium::timer_system<std::chrono::milliseconds> &sys,
             const sodium::stream<Direction> &instructions,
             const sodium::stream<sodium::unit> &pause,
             const sodium::cell<bool> &windowValid)
    : sys_{sys}, score_{0}, snake_{Snake(0, 0)}, prey_{Point(0, 0)},
      terminated_{false} {
  sodium::transaction t{};

  sodium::cell_loop<bool> gameValid;
  const auto moveS = movementTicks(
      instructions, pause,
      gameValid.lift(windowValid,
                     [](const auto gv, const auto wv) { return gv && wv; }));

  sodium::cell_loop<Point> prey;
  sodium::cell_loop<Snake> snake;

  const auto nextSnake = moveS.snapshot(
      prey, snake, [](const auto &d, const auto &p, const auto &s) {
        return s.moveOrGrow(d, p);
      });
  snake.loop(nextSnake.map([](const auto &p) { return p.first; })
                 .hold(Snake(fieldWidth, fieldHeight)));
  snake_ = snake;

  const auto eat = nextSnake.filter([](const auto &p) { return p.second; })
                       .map([](const auto &p) { return p.first; });
  prey.loop(sodium::filter_optional(
                eat.map([](const auto &s) { return s.nextPrey(); }))
                .hold(Point(10, 10)));
  prey_ = prey;

  terminated_ =
      nextSnake.map([](const auto &p) { return p.first.selfIntersecting(); })
          .filter([](const auto b) { return b; })
          .once()
          .hold(false);
  gameValid.loop(terminated_.map([](const auto b) { return !b; }));
  score_ = eat.accum<int>(0, [](const auto &, const auto s) { return s + 1; });
}

const sodium::stream<std::chrono::milliseconds>
Logic::ticker(const sodium::cell<std::chrono::milliseconds> interval) const {
  sodium::transaction t{};
  auto tiem = sodium::cell_loop<std::chrono::milliseconds>();
  const auto s = sys_.at(tiem.lift(interval, [](const auto &t, const auto &T) {
    return maybe<std::chrono::milliseconds>(t + T);
  }));
  tiem.loop(s.hold(sys_.time.sample()));
  return s;
}

const sodium::stream<Point>
Logic::movementTicks(const sodium::stream<Direction> &instructions,
                     const sodium::stream<sodium::unit> &pause,
                     const sodium::cell<bool> &valid) const {
  using namespace std::chrono;
  using namespace std::chrono_literals;

  const auto paused =
      pause.accum<bool>(false, [](const auto, const auto b) { return !b; });

  const auto direction =
      instructions.map([](const auto &d) { return directionToPoint(d); })
          .gate(paused.map([](const auto &b) { return !b; }))
          .hold(RIGHT);

  const auto ticks =
      ticker(sodium::cell<milliseconds>(150ms))
          .gate(paused.lift(
              valid, [](const auto p, const auto v) { return !p && v; }));

  return ticks
      .snapshot(direction, [](const auto &, const auto &d) { return d; })
      .collect(maybe<Point>(), [](const auto &d, const auto &s) {
        maybe<Point> dir(d);
        auto opposite = dir.map([](const auto &d) { return d.reverse(); });
        return s == dir || s == opposite ? std::make_pair(s.get(), s)
                                         : std::make_pair(d, dir);
      });
}