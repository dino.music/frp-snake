#include <Logic/Point.hpp>

Point::Point(int x_, int y_) : x{x_}, y{y_} {}

const Point Point::operator+(const Point &other) const {
  return Point(x + other.x, y + other.y);
}

bool Point::operator==(const Point &other) const {
  return x == other.x && y == other.y;
}

const Point Point::reverse() const { return Point(-x, -y); }

const Point UP = Point(0, -1);
const Point DOWN = Point(0, 1);
const Point RIGHT = Point(1, 0);
const Point LEFT = Point(-1, 0);