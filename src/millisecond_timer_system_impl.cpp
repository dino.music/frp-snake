#include <millisecond_timer_system_impl.hpp>

millisecond_timer_system_impl::millisecond_timer_system_impl()
    : next_seq(0), initTime_{time()} {
  t = std::thread([this]() {
    while (!finished.load()) {
      auto t = time();
      now_.store(t);
      boost::optional<entry> oe;
      while (true) {
        {
          oe = entries.pop_if([t](const entry &e) { return e.t <= t; });
        }
        if (oe)
          oe.get().callback();
        else {
          std::this_thread::sleep_for(std::chrono::milliseconds{75});
          break;
        }
      }
    }
  });
}

millisecond_timer_system_impl::~millisecond_timer_system_impl() {
  finished.exchange(true);
  t.join();
}

std::function<void()>
millisecond_timer_system_impl::set_timer(std::chrono::milliseconds t,
                                         std::function<void()> callback) {

  entry e(t, callback, ++next_seq);
  entries.push(e);
  return [this, e]() { entries.remove(e); };
}

std::chrono::milliseconds millisecond_timer_system_impl::now() {
  return now_.load() - initTime_;
}

std::chrono::milliseconds millisecond_timer_system_impl::time() const {
  return std::chrono::duration_cast<std::chrono::milliseconds>(
      std::chrono::high_resolution_clock::now().time_since_epoch());
}