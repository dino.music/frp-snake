#include <Render/TBRenderer.hpp>
#include <termbox.h>

void TBRenderer::drawBlock(const int x_, const int y_, const int clr,
                           const char symbol) const {
  const auto [x, y] =
      data.orientation == Direction::Up ? std::tie(x_, y_) : std::tie(y_, x_);
  const int bgClr = data.displaySymbol ? TB_BLACK : clr;
  const char s = data.displaySymbol ? symbol : ' ';
  for (int i = 0; i < data.cellsPerBlockX; ++i)
    for (int j = 0; j < data.cellsPerBlockY; ++j)
      tb_change_cell(data.offsetX + data.cellsPerBlockX +
                         x * data.cellsPerBlockX + i,
                     data.offsetY + data.headerHeight - 1 +
                         data.cellsPerBlockY + y * data.cellsPerBlockY + j,
                     s, clr, bgClr);
}

void TBRenderer::drawWalls() const {
  const auto [fW, fH] = data.orientation == Direction::Up
                            ? std::tie(data.frameWidth, data.frameHeight)
                            : std::tie(data.frameHeight, data.frameWidth);
  // North wall
  drawWall(data.offsetX,                            //
           data.offsetX + fW * data.cellsPerBlockX, //
           data.offsetY + data.headerHeight - 1,    //
           data.offsetY + data.headerHeight - 1 + data.cellsPerBlockY);
  // South wall
  drawWall(data.offsetX,                            //
           data.offsetX + fW * data.cellsPerBlockX, //
           data.offsetY + data.headerHeight - 1 +
               (fH - 1) * data.cellsPerBlockY, //
           data.offsetY + data.headerHeight - 1 + fH * data.cellsPerBlockY);
  // West wall
  drawWall(data.offsetX,                         //
           data.offsetX + data.cellsPerBlockX,   //
           data.offsetY + data.headerHeight - 1, //
           data.offsetY + data.headerHeight - 1 + fH * data.cellsPerBlockY);
  // Berlin wall
  drawWall(data.offsetX + (fW - 1) * data.cellsPerBlockX, //
           data.offsetX + fW * data.cellsPerBlockX,       //
           data.offsetY + data.headerHeight - 1,          //
           data.offsetY + data.headerHeight - 1 + fH * data.cellsPerBlockY);
}

void TBRenderer::drawHeader(const int s) const {
  const auto score = std::to_string(s);
  const int offsetX = data.w / 2 - score.length() / 2;
  const int offsetY = std::max((data.headerHeight + data.offsetY) / 2 - 1, 1);
  for (int i = 0; i < score.length(); ++i)
    tb_change_cell(offsetX + i, offsetY, score[i], TB_WHITE, TB_BLACK);
}

void TBRenderer::drawWall(const int x1, const int x2, const int y1,
                          const int y2) const {
  for (int i = x1; i < x2; ++i)
    for (int j = y1; j < y2; ++j)
      tb_change_cell(i, j, ' ', TB_WHITE, TB_WHITE);
}