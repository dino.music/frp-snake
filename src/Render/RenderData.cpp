#include <Render/RenderData.hpp>

RenderData RenderData::calcData(const int w, const int h) {
  RenderData data;
  data.h = h;
  data.w = w;
  if (w >= frameWidth && h >= frameHeight) {
    data.initData(frameWidth, w, frameHeight, h - headerHeight);
    data.orientation = Direction::Up;
  } else if (w >= frameHeight && h >= frameWidth) {
    data.initData(frameHeight, w, frameWidth, h);
    data.orientation = Direction::Left;

  } else {
    data.displaySymbol = true;
    data.valid = false;
  }
  return data;
}

void RenderData::initData(const int fW, const int w, const int fH,
                          const int h) {
  auto [x, y] = std::make_pair(w / fW, h / fH);
  std::tie(cellsPerBlockX, cellsPerBlockY) =
      x >= (2 * y) ? std::make_pair(2 * y, y)
                   : std::make_pair(std::max(x / 2, 1), std::max(x / 2, 1));
  offsetX = (w - fW * cellsPerBlockX) / 2;
  offsetY = (h - fH * cellsPerBlockY) / 2;
  displaySymbol = (cellsPerBlockX == 1);
}