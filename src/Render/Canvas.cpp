#include <Render/Canvas.hpp>

Canvas::Canvas(const sodium::stream<Dimensions> &terminalSize,
               const Dimensions &initSize)
    : renderer_{RenderData{}}, valid_{true}, orientation_{Direction::Up} {
  const auto initData = RenderData::calcData(initSize.first, initSize.second);

  auto renderData = terminalSize.map(
      [](const auto &wh) { return RenderData::calcData(wh.first, wh.second); });

  renderer_ = renderData.map([](const auto &d) { return TBRenderer(d); })
                  .hold(TBRenderer(initData));

  valid_ = renderData.map([](const auto &d) { return d.valid; })
               .hold(initData.valid);

  orientation_ =
      sodium::filter_optional(
          renderData.map([](const auto &d) { return d.orientation; })
              .collect(maybe<Direction>(),
                       [](const auto &o, const auto &s) {
                         auto mo = maybe<Direction>(o);
                         return mo == s ? std::make_pair(maybe<Direction>(), s)
                                        : std::make_pair(mo, mo);
                       }))
          .hold(Direction::Up);
}