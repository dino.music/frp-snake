#include <Game.hpp>
#include <TBGuard.hpp>
#include <thread>

int main(int argc, char const *argv[]) {
  std::srand(std::time(nullptr));
  TBGuard tbGuard;

  auto inputSink = sodium::stream_sink<char>();
  auto windowSizeSink = sodium::stream_sink<std::pair<int, int>>();
  Game game(inputSink, windowSizeSink);

  while (true) {
    struct tb_event inputEvent;
    int eventType = tb_peek_event(&inputEvent, 0);
    if (eventType == TB_EVENT_KEY) {
      if (inputEvent.ch == 'q')
        break;
      inputSink.send(inputEvent.ch);
    } else if (eventType == TB_EVENT_RESIZE) {
      windowSizeSink.send(std::make_pair(inputEvent.w, inputEvent.h));
    }
    game.render().sample()();
    std::this_thread::sleep_for(std::chrono::milliseconds{17});
  }
  return 0;
}
