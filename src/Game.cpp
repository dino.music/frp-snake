#include <Game.hpp>
#include <termbox.h>

Game::Game(const sodium::stream<char> &inputs,
           const sodium::stream<Dimensions> &termDim)
    : sys_{std::make_shared<millisecond_timer_system_impl>()},
      canvas_{termDim, std::make_pair(tb_width(), tb_height())}, render_{
                                                                     [] {}} {
  sodium::transaction t{};

  auto lowercaseInputs = inputs.map([](auto ch) { return tolower(ch); });

  auto keyMapping = canvas_.orientation().map([](const auto o) {
    return o == Direction::Up ? standardMapping : rotatedMapping;
  });
  instructions_ = sodium::filter_optional(lowercaseInputs.snapshot(
      keyMapping, [](const auto ch, const auto &mapping) {
        const auto it = mapping.find(ch);
        return it == mapping.cend() ? maybe<Direction>()
                                    : maybe<Direction>(it->second);
      }));

  pause_ =
      inputs.filter([](auto ch) { return ch == 'p'; }).map([](const auto _) {
        return sodium::unit();
      });

  auto resetRequest =
      inputs.filter([](const auto ch) { return std::tolower(ch) == 'n'; })
          .map([](const auto) { return sodium::unit(); });
  sodium::cell_loop<bool> resetAvailable;
  auto reset = resetRequest.gate(resetAvailable);
  sodium::cell<Logic> logic =
      reset
          .map([this](const auto &) {
            return Logic(sys_, instructions_, pause_, canvas_.valid());
          })
          .hold(Logic(sys_, instructions_, pause_, canvas_.valid()));
  resetAvailable.loop(sodium::switch_c(
      logic.map([](const auto &l) { return l.terminated(); })));

  auto scene = sodium::switch_c(logic.map([](const auto &l) {
    return l.snake()
        .lift(l.prey(), [](const auto &s,
                           const auto &p) { return std::make_tuple(s, p); })
        .lift(l.score(), [](const auto &f, const auto &s) {
          return std::tuple_cat(f, std::make_tuple(s));
        });
  }));
  render_ =
      scene.lift(canvas_.renderer(),
                 [](const auto &s, const auto &r) -> std::function<void()> {
                   return [=]() {
                     tb_clear();

                     r.drawHeader(std::get<2>(s));

                     r.drawWalls();

                     const auto prey = std::get<1>(s);
                     r.drawBlock(prey.x, prey.y, TB_RED, 'O');

                     for (const auto &segment : std::get<0>(s).getSegments())
                       r.drawBlock(segment.x, segment.y, TB_WHITE, '@');

                     const auto head = std::get<0>(s).getSegments().back();
                     r.drawBlock(head.x, head.y, TB_YELLOW, '@');

                     tb_present();
                   };
                 });
}

const std::unordered_map<char, Direction> Game::standardMapping{
    {'w', Direction::Up},
    {'a', Direction::Left},
    {'s', Direction::Down},
    {'d', Direction::Right}};

const std::unordered_map<char, Direction> Game::rotatedMapping{
    {'w', Direction::Left},
    {'a', Direction::Up},
    {'s', Direction::Right},
    {'d', Direction::Down}};