Building requires the Meson and ninja build systems.
A version of Meson that supports the CMake plugin is required.
To build, in the top level directory perform:
	mkdir build 
	cd build 
	meson .. 
	ninja
